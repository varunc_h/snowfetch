#include <stdio.h>
#include <stdlib.h>
#include <sysinfo.h>

int main(int argc, char* argv[]) {
     (void)argc;
     (void)argv;
     printf("snowfetch\n");
     value_t username = get_user_name();
     printf("%s\n", username.val);
     free(username.val);
     return EXIT_SUCCESS;
 }
