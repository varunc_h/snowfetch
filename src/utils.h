#pragma once
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>

typedef struct value_t {
    char       *val;
    uint8_t    size;
    bool       valid;
} value_t;

#define make_value()        \
    {                       \
        .val = NULL,        \
        .size = 0,          \
        .valid = false,     \
    };

value_t read_env(const char *key);
