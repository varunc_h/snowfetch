#include "utils.h"

value_t read_env(const char *key) {
    value_t env = make_value();
    const char *temp = getenv(key);
    if(temp == NULL) {
        return env;
    }
    env.val = strdup(temp);
    if(env.val != NULL) {
        env.valid = true;
        env.size = strlen(env.val);
    }
    return env;
}
